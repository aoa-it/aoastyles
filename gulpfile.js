var gulp = require('gulp');
var connect = require('gulp-connect');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var rename = require('gulp-rename');

gulp.task('webserver', function() {
  connect.server({
    livereload: true,
    port: 8000,
  });
});

gulp.task('sass', function() {
  return gulp.src('./sass/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('guide.css'))
    .pipe(gulp.dest('./styles'))
    .pipe(connect.reload());
});

gulp.task('html', function() {
  gulp.src('*.html')
    .pipe(connect.reload());
});

gulp.task('browser-sync', function() {
  browserSync.init({
    server: {
      baseDir: './',
    },
  });
});

gulp.task('watch', ['html'], function() {
  gulp.watch(['*.html'], ['html']);
  gulp.watch(['./sass/**/*.scss'], ['sass']);
});

gulp.task('default', ['webserver', 'watch']);
